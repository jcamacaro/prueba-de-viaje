-- MySQL dump 10.13  Distrib 5.6.24, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: don_quintin_template
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.21-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auditoria`
--

DROP TABLE IF EXISTS `auditoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auditoria` (
  `idAuditoria` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `fecha` varchar(45) NOT NULL DEFAULT 'CURRENT_TIMESTAMP',
  PRIMARY KEY (`idAuditoria`),
  KEY `kk_auditoria_1_idx` (`idUsuario`),
  CONSTRAINT `fk_auditoria_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auditoria`
--

LOCK TABLES `auditoria` WRITE;
/*!40000 ALTER TABLE `auditoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `auditoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('e5tduidqfbs7ltl6qbood15gv2ivt8ln','::1',1536436328,'__ci_last_regenerate|i:1536436328;id|s:1:\"1\";id_rol_usuario|s:1:\"1\";username|s:5:\"admin\";email|s:17:\"admin@hotmail.com\";is_logged_in|b:1;'),('pqmd41bbiu9na4ueiokbp1kk6k1b5t48','::1',1536437319,'__ci_last_regenerate|i:1536437319;id|s:1:\"1\";id_rol_usuario|s:1:\"1\";username|s:5:\"admin\";email|s:17:\"admin@hotmail.com\";menu|a:3:{i:0;O:8:\"stdClass\":14:{s:15:\"idPermisosRoles\";s:1:\"1\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"1\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"1\";s:11:\"descripcion\";s:15:\"ADMINISTRACIÓN\";s:5:\"icono\";s:19:\"fa fa-user-circle-o\";s:3:\"url\";s:1:\"#\";s:5:\"orden\";s:1:\"0\";s:9:\"jerarquia\";s:1:\"0\";s:4:\"menu\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 14:42:45\";}i:1;O:8:\"stdClass\":14:{s:15:\"idPermisosRoles\";s:1:\"2\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"2\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"2\";s:11:\"descripcion\";s:7:\"USUARIO\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:5:\"orden\";s:1:\"0\";s:9:\"jerarquia\";s:1:\"1\";s:4:\"menu\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:21\";}i:2;O:8:\"stdClass\":14:{s:15:\"idPermisosRoles\";s:1:\"3\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"3\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"3\";s:11:\"descripcion\";s:6:\"LISTAR\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:5:\"orden\";s:1:\"0\";s:9:\"jerarquia\";s:1:\"2\";s:4:\"menu\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:57\";}}is_logged_in|b:1;'),('lc97rqnv2rpc1rpuibtec4f7ehn0t6vh','::1',1536439320,'__ci_last_regenerate|i:1536439320;id|s:1:\"1\";id_rol_usuario|s:1:\"1\";username|s:5:\"admin\";email|s:17:\"admin@hotmail.com\";menu|a:3:{i:0;O:8:\"stdClass\":14:{s:15:\"idPermisosRoles\";s:1:\"1\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"1\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"1\";s:11:\"descripcion\";s:15:\"ADMINISTRACIÓN\";s:5:\"icono\";s:19:\"fa fa-user-circle-o\";s:3:\"url\";s:1:\"#\";s:5:\"orden\";s:1:\"0\";s:9:\"jerarquia\";s:1:\"0\";s:4:\"menu\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 14:42:45\";}i:1;O:8:\"stdClass\":14:{s:15:\"idPermisosRoles\";s:1:\"2\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"2\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"2\";s:11:\"descripcion\";s:7:\"USUARIO\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:5:\"orden\";s:1:\"0\";s:9:\"jerarquia\";s:1:\"1\";s:4:\"menu\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:21\";}i:2;O:8:\"stdClass\":14:{s:15:\"idPermisosRoles\";s:1:\"3\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"3\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"3\";s:11:\"descripcion\";s:6:\"LISTAR\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:5:\"orden\";s:1:\"0\";s:9:\"jerarquia\";s:1:\"2\";s:4:\"menu\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:57\";}}is_logged_in|b:1;'),('6jj458qipuhk73pff6nn2nj8bm6lmf30','::1',1536443991,'__ci_last_regenerate|i:1536443991;id|s:1:\"1\";id_rol_usuario|s:1:\"1\";username|s:5:\"admin\";email|s:17:\"admin@hotmail.com\";menu|a:3:{i:0;O:8:\"stdClass\":14:{s:15:\"idPermisosRoles\";s:1:\"1\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"1\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"1\";s:11:\"descripcion\";s:15:\"ADMINISTRACIÓN\";s:5:\"icono\";s:19:\"fa fa-user-circle-o\";s:3:\"url\";s:1:\"#\";s:5:\"orden\";s:1:\"0\";s:9:\"jerarquia\";s:1:\"0\";s:4:\"menu\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 14:42:45\";}i:1;O:8:\"stdClass\":14:{s:15:\"idPermisosRoles\";s:1:\"2\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"2\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"2\";s:11:\"descripcion\";s:7:\"USUARIO\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:5:\"orden\";s:1:\"0\";s:9:\"jerarquia\";s:1:\"1\";s:4:\"menu\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:21\";}i:2;O:8:\"stdClass\":14:{s:15:\"idPermisosRoles\";s:1:\"3\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"3\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"3\";s:11:\"descripcion\";s:6:\"LISTAR\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:5:\"orden\";s:1:\"0\";s:9:\"jerarquia\";s:1:\"2\";s:4:\"menu\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:57\";}}is_logged_in|b:1;'),('gg0j96soapm957avraiu9jml9a2strdg','::1',1536444652,'__ci_last_regenerate|i:1536444652;id|s:1:\"1\";id_rol_usuario|s:1:\"1\";username|s:5:\"admin\";email|s:17:\"admin@hotmail.com\";menu|a:4:{i:0;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"1\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"1\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"1\";s:8:\"parentId\";N;s:11:\"descripcion\";s:15:\"ADMINISTRACIÓN\";s:5:\"icono\";s:19:\"fa fa-user-circle-o\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"0\";s:9:\"create_at\";s:19:\"2018-09-08 14:42:45\";}i:1;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"2\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"2\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";s:19:\"2018-09-08 17:26:29\";s:13:\"idMenuSistema\";s:1:\"2\";s:8:\"parentId\";s:1:\"1\";s:11:\"descripcion\";s:7:\"USUARIO\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:21\";}i:2;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"3\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"3\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";s:19:\"2018-09-08 17:26:29\";s:13:\"idMenuSistema\";s:1:\"3\";s:8:\"parentId\";s:1:\"1\";s:11:\"descripcion\";s:6:\"LISTAR\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"2\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:57\";}i:3;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"4\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"4\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 17:35:13\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"4\";s:8:\"parentId\";N;s:11:\"descripcion\";s:11:\"OPERACIONES\";s:5:\"icono\";s:19:\"fa fa-user-circle-o\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"0\";s:9:\"create_at\";s:19:\"2018-09-08 17:30:51\";}}is_logged_in|b:1;'),('ua43lalpj39t3hu02lspfqptrl1pauqt','::1',1536444792,'__ci_last_regenerate|i:1536444783;id|s:1:\"1\";id_rol_usuario|s:1:\"1\";username|s:5:\"admin\";email|s:17:\"admin@hotmail.com\";menu|a:6:{i:0;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"1\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"1\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"1\";s:8:\"parentId\";N;s:11:\"descripcion\";s:15:\"ADMINISTRACIÓN\";s:5:\"icono\";s:19:\"fa fa-user-circle-o\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"0\";s:9:\"create_at\";s:19:\"2018-09-08 14:42:45\";}i:1;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"2\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"2\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";s:19:\"2018-09-08 17:26:29\";s:13:\"idMenuSistema\";s:1:\"2\";s:8:\"parentId\";s:1:\"1\";s:11:\"descripcion\";s:7:\"USUARIO\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:21\";}i:2;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"3\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"3\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";s:19:\"2018-09-08 17:38:34\";s:13:\"idMenuSistema\";s:1:\"3\";s:8:\"parentId\";s:1:\"2\";s:11:\"descripcion\";s:6:\"LISTAR\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"2\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:57\";}i:3;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"4\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"4\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 17:35:13\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"4\";s:8:\"parentId\";N;s:11:\"descripcion\";s:11:\"OPERACIONES\";s:5:\"icono\";s:19:\"fa fa-user-circle-o\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"0\";s:9:\"create_at\";s:19:\"2018-09-08 17:30:51\";}i:4;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"5\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"5\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 17:40:47\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"5\";s:8:\"parentId\";s:1:\"4\";s:11:\"descripcion\";s:5:\"LOTES\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 17:38:34\";}i:5;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"6\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"6\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 17:40:47\";s:9:\"update_at\";s:19:\"2018-09-08 17:42:51\";s:13:\"idMenuSistema\";s:1:\"6\";s:8:\"parentId\";s:1:\"1\";s:11:\"descripcion\";s:6:\"MENÚS\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 17:38:34\";}}is_logged_in|b:1;'),('p31be80tu07bm4ecjv0efdkna2j3dhhg','::1',1536446704,'__ci_last_regenerate|i:1536446704;id|s:1:\"1\";id_rol_usuario|s:1:\"1\";username|s:5:\"admin\";email|s:17:\"admin@hotmail.com\";menu|a:6:{i:0;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"1\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"1\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"1\";s:8:\"parentId\";N;s:11:\"descripcion\";s:15:\"ADMINISTRACIÓN\";s:5:\"icono\";s:19:\"fa fa-user-circle-o\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"0\";s:9:\"create_at\";s:19:\"2018-09-08 14:42:45\";}i:1;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"2\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"2\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";s:19:\"2018-09-08 17:26:29\";s:13:\"idMenuSistema\";s:1:\"2\";s:8:\"parentId\";s:1:\"1\";s:11:\"descripcion\";s:7:\"USUARIO\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:21\";}i:2;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"3\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"3\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";s:19:\"2018-09-08 17:38:34\";s:13:\"idMenuSistema\";s:1:\"3\";s:8:\"parentId\";s:1:\"2\";s:11:\"descripcion\";s:6:\"LISTAR\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"2\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:57\";}i:3;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"4\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"4\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 17:35:13\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"4\";s:8:\"parentId\";N;s:11:\"descripcion\";s:11:\"OPERACIONES\";s:5:\"icono\";s:19:\"fa fa-user-circle-o\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"0\";s:9:\"create_at\";s:19:\"2018-09-08 17:30:51\";}i:4;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"5\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"5\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 17:40:47\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"5\";s:8:\"parentId\";s:1:\"4\";s:11:\"descripcion\";s:5:\"LOTES\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 17:38:34\";}i:5;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"6\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"6\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 17:40:47\";s:9:\"update_at\";s:19:\"2018-09-08 17:58:46\";s:13:\"idMenuSistema\";s:1:\"6\";s:8:\"parentId\";s:1:\"1\";s:11:\"descripcion\";s:6:\"MENÚS\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:11:\"CMenu/index\";s:9:\"jerarquia\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 17:38:34\";}}is_logged_in|b:1;'),('cd80nkefhtl2nape68niu1qojf1g7312','::1',1536447126,'__ci_last_regenerate|i:1536447126;id|s:1:\"1\";id_rol_usuario|s:1:\"1\";username|s:5:\"admin\";email|s:17:\"admin@hotmail.com\";menu|a:6:{i:0;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"1\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"1\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"1\";s:8:\"parentId\";N;s:11:\"descripcion\";s:15:\"ADMINISTRACIÓN\";s:5:\"icono\";s:19:\"fa fa-user-circle-o\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"0\";s:9:\"create_at\";s:19:\"2018-09-08 14:42:45\";}i:1;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"2\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"2\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";s:19:\"2018-09-08 17:26:29\";s:13:\"idMenuSistema\";s:1:\"2\";s:8:\"parentId\";s:1:\"1\";s:11:\"descripcion\";s:7:\"USUARIO\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:21\";}i:2;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"3\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"3\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";s:19:\"2018-09-08 17:38:34\";s:13:\"idMenuSistema\";s:1:\"3\";s:8:\"parentId\";s:1:\"2\";s:11:\"descripcion\";s:6:\"LISTAR\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"2\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:57\";}i:3;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"4\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"4\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 17:35:13\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"4\";s:8:\"parentId\";N;s:11:\"descripcion\";s:11:\"OPERACIONES\";s:5:\"icono\";s:19:\"fa fa-user-circle-o\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"0\";s:9:\"create_at\";s:19:\"2018-09-08 17:30:51\";}i:4;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"5\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"5\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 17:40:47\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"5\";s:8:\"parentId\";s:1:\"4\";s:11:\"descripcion\";s:5:\"LOTES\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 17:38:34\";}i:5;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"6\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"6\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 17:40:47\";s:9:\"update_at\";s:19:\"2018-09-08 17:58:46\";s:13:\"idMenuSistema\";s:1:\"6\";s:8:\"parentId\";s:1:\"1\";s:11:\"descripcion\";s:6:\"MENÚS\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:11:\"CMenu/index\";s:9:\"jerarquia\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 17:38:34\";}}is_logged_in|b:1;'),('3hhbhg0d2lqacs97oiii1033gm396qf0','::1',1536447583,'__ci_last_regenerate|i:1536447583;id|s:1:\"1\";id_rol_usuario|s:1:\"1\";username|s:5:\"admin\";email|s:17:\"admin@hotmail.com\";menu|a:6:{i:0;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"1\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"1\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"1\";s:8:\"parentId\";N;s:11:\"descripcion\";s:15:\"ADMINISTRACIÓN\";s:5:\"icono\";s:19:\"fa fa-user-circle-o\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"0\";s:9:\"create_at\";s:19:\"2018-09-08 14:42:45\";}i:1;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"2\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"2\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";s:19:\"2018-09-08 17:26:29\";s:13:\"idMenuSistema\";s:1:\"2\";s:8:\"parentId\";s:1:\"1\";s:11:\"descripcion\";s:7:\"USUARIO\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:21\";}i:2;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"3\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"3\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";s:19:\"2018-09-08 17:38:34\";s:13:\"idMenuSistema\";s:1:\"3\";s:8:\"parentId\";s:1:\"2\";s:11:\"descripcion\";s:6:\"LISTAR\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"2\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:57\";}i:3;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"4\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"4\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 17:35:13\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"4\";s:8:\"parentId\";N;s:11:\"descripcion\";s:11:\"OPERACIONES\";s:5:\"icono\";s:19:\"fa fa-user-circle-o\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"0\";s:9:\"create_at\";s:19:\"2018-09-08 17:30:51\";}i:4;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"5\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"5\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 17:40:47\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"5\";s:8:\"parentId\";s:1:\"4\";s:11:\"descripcion\";s:5:\"LOTES\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 17:38:34\";}i:5;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"6\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"6\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 17:40:47\";s:9:\"update_at\";s:19:\"2018-09-08 17:58:46\";s:13:\"idMenuSistema\";s:1:\"6\";s:8:\"parentId\";s:1:\"1\";s:11:\"descripcion\";s:6:\"MENÚS\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:11:\"CMenu/index\";s:9:\"jerarquia\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 17:38:34\";}}is_logged_in|b:1;'),('rbvd0j3j5pei56jvbviu7uvduclqva83','::1',1536447593,'__ci_last_regenerate|i:1536447589;id|s:1:\"1\";id_rol_usuario|s:1:\"1\";username|s:5:\"admin\";email|s:17:\"admin@hotmail.com\";menu|a:4:{i:0;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"1\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"1\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"1\";s:8:\"parentId\";N;s:11:\"descripcion\";s:15:\"ADMINISTRACIÓN\";s:5:\"icono\";s:19:\"fa fa-user-circle-o\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"0\";s:9:\"create_at\";s:19:\"2018-09-08 14:42:45\";}i:1;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"2\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"2\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";s:19:\"2018-09-08 17:26:29\";s:13:\"idMenuSistema\";s:1:\"2\";s:8:\"parentId\";s:1:\"1\";s:11:\"descripcion\";s:7:\"USUARIO\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:21\";}i:2;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"3\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"3\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";s:19:\"2018-09-08 17:38:34\";s:13:\"idMenuSistema\";s:1:\"3\";s:8:\"parentId\";s:1:\"2\";s:11:\"descripcion\";s:6:\"LISTAR\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"2\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:57\";}i:3;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"6\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"6\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 17:40:47\";s:9:\"update_at\";s:19:\"2018-09-08 17:58:46\";s:13:\"idMenuSistema\";s:1:\"6\";s:8:\"parentId\";s:1:\"1\";s:11:\"descripcion\";s:6:\"MENÚS\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:11:\"CMenu/index\";s:9:\"jerarquia\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 17:38:34\";}}is_logged_in|b:1;'),('cdgrrpvptlh6n3ih7cuvm8oo4k21qhad','::1',1536448512,'__ci_last_regenerate|i:1536448512;id|s:1:\"1\";id_rol_usuario|s:1:\"1\";username|s:5:\"admin\";email|s:17:\"admin@hotmail.com\";menu|a:4:{i:0;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"1\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"1\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"1\";s:8:\"parentId\";N;s:11:\"descripcion\";s:15:\"ADMINISTRACIÓN\";s:5:\"icono\";s:19:\"fa fa-user-circle-o\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"0\";s:9:\"create_at\";s:19:\"2018-09-08 14:42:45\";}i:1;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"2\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"2\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";s:19:\"2018-09-08 17:26:29\";s:13:\"idMenuSistema\";s:1:\"2\";s:8:\"parentId\";s:1:\"1\";s:11:\"descripcion\";s:7:\"USUARIO\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:21\";}i:2;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"3\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"3\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";s:19:\"2018-09-08 17:38:34\";s:13:\"idMenuSistema\";s:1:\"3\";s:8:\"parentId\";s:1:\"2\";s:11:\"descripcion\";s:6:\"LISTAR\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"2\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:57\";}i:3;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"6\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"6\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 17:40:47\";s:9:\"update_at\";s:19:\"2018-09-08 17:58:46\";s:13:\"idMenuSistema\";s:1:\"6\";s:8:\"parentId\";s:1:\"1\";s:11:\"descripcion\";s:6:\"MENÚS\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:11:\"CMenu/index\";s:9:\"jerarquia\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 17:38:34\";}}is_logged_in|b:1;'),('issar7d1iuqa0efi85u80fklrm2fd83u','::1',1536448516,'__ci_last_regenerate|i:1536448512;id|s:1:\"1\";id_rol_usuario|s:1:\"1\";username|s:5:\"admin\";email|s:17:\"admin@hotmail.com\";menu|a:4:{i:0;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"1\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"1\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";N;s:13:\"idMenuSistema\";s:1:\"1\";s:8:\"parentId\";N;s:11:\"descripcion\";s:15:\"ADMINISTRACIÓN\";s:5:\"icono\";s:19:\"fa fa-user-circle-o\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"0\";s:9:\"create_at\";s:19:\"2018-09-08 14:42:45\";}i:1;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"2\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"2\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";s:19:\"2018-09-08 17:26:29\";s:13:\"idMenuSistema\";s:1:\"2\";s:8:\"parentId\";s:1:\"1\";s:11:\"descripcion\";s:7:\"USUARIO\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:21\";}i:2;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"3\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"3\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 14:49:14\";s:9:\"update_at\";s:19:\"2018-09-08 17:38:34\";s:13:\"idMenuSistema\";s:1:\"3\";s:8:\"parentId\";s:1:\"2\";s:11:\"descripcion\";s:6:\"LISTAR\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:1:\"#\";s:9:\"jerarquia\";s:1:\"2\";s:9:\"create_at\";s:19:\"2018-09-08 14:43:57\";}i:3;O:8:\"stdClass\":13:{s:15:\"idPermisosRoles\";s:1:\"6\";s:5:\"rolId\";s:1:\"1\";s:13:\"menuSistemaId\";s:1:\"6\";s:6:\"estado\";s:1:\"1\";s:10:\"created_at\";s:19:\"2018-09-08 17:40:47\";s:9:\"update_at\";s:19:\"2018-09-08 17:58:46\";s:13:\"idMenuSistema\";s:1:\"6\";s:8:\"parentId\";s:1:\"1\";s:11:\"descripcion\";s:6:\"MENÚS\";s:5:\"icono\";s:23:\"fa fa-fw fa-plus-square\";s:3:\"url\";s:11:\"CMenu/index\";s:9:\"jerarquia\";s:1:\"1\";s:9:\"create_at\";s:19:\"2018-09-08 17:38:34\";}}is_logged_in|b:1;');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menusistema`
--

DROP TABLE IF EXISTS `menusistema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menusistema` (
  `idMenuSistema` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT NULL,
  `descripcion` varchar(50) NOT NULL,
  `icono` varchar(50) NOT NULL DEFAULT 'fa fa-user-circle-o',
  `url` varchar(50) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT '1',
  `jerarquia` enum('0','1','2','3') NOT NULL COMMENT '0 si es cabecera\n1 padre\n2 hijo\n3 acciones del hijo\nejemplo \nADMINISTRACION(0) cabecera\nUSUARIO(1) menu\nListar(2) sub menu\nAccion(3)->editar, eliminar, cambiar permisos, etc\n',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idMenuSistema`),
  KEY `fk_menusistema_1_idx` (`parentId`),
  CONSTRAINT `fk_menusistema_1` FOREIGN KEY (`parentId`) REFERENCES `menusistema` (`idMenuSistema`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menusistema`
--

LOCK TABLES `menusistema` WRITE;
/*!40000 ALTER TABLE `menusistema` DISABLE KEYS */;
INSERT INTO `menusistema` VALUES (1,NULL,'ADMINISTRACIÓN','fa fa-user-circle-o','#',1,'0','2018-09-08 19:12:45',NULL),(2,1,'USUARIO','fa fa-fw fa-plus-square','#',1,'1','2018-09-08 19:13:21','2018-09-08 21:56:29'),(3,2,'LISTAR','fa fa-fw fa-plus-square','#',1,'2','2018-09-08 19:13:57','2018-09-08 22:08:34'),(4,NULL,'OPERACIONES','fa fa-user-circle-o','#',0,'0','2018-09-08 22:00:51','2018-09-08 22:59:38'),(5,4,'LOTES','fa fa-fw fa-plus-square','#',0,'1','2018-09-08 22:08:34','2018-09-08 22:59:38'),(6,1,'MENÚS','fa fa-fw fa-plus-square','CMenu/index',1,'1','2018-09-08 22:08:34','2018-09-08 22:28:46');
/*!40000 ALTER TABLE `menusistema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permisosroles`
--

DROP TABLE IF EXISTS `permisosroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permisosroles` (
  `idPermisosRoles` int(11) NOT NULL AUTO_INCREMENT,
  `rolId` int(11) NOT NULL,
  `menuSistemaId` int(11) NOT NULL,
  `estado` int(2) NOT NULL DEFAULT '1' COMMENT '1= activo\n0= inactivo',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idPermisosRoles`),
  KEY `fk_permisosroles_1_idx` (`menuSistemaId`),
  KEY `fk_permisosroles_2_idx` (`rolId`),
  CONSTRAINT `fk_permisosroles_1` FOREIGN KEY (`menuSistemaId`) REFERENCES `menusistema` (`idMenuSistema`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_permisosroles_2` FOREIGN KEY (`rolId`) REFERENCES `roles` (`idRol`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permisosroles`
--

LOCK TABLES `permisosroles` WRITE;
/*!40000 ALTER TABLE `permisosroles` DISABLE KEYS */;
INSERT INTO `permisosroles` VALUES (1,1,1,1,'2018-09-08 19:19:14',NULL),(2,1,2,1,'2018-09-08 19:19:14',NULL),(3,1,3,1,'2018-09-08 19:19:14',NULL),(4,1,4,1,'2018-09-08 22:05:13',NULL),(5,1,5,1,'2018-09-08 22:10:47',NULL),(6,1,6,1,'2018-09-08 22:10:47',NULL);
/*!40000 ALTER TABLE `permisosroles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permisosusuarios`
--

DROP TABLE IF EXISTS `permisosusuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permisosusuarios` (
  `IdPermisosUsuarios` int(11) NOT NULL AUTO_INCREMENT,
  `usuarioId` int(11) NOT NULL,
  `menuSistemaId` int(11) NOT NULL,
  `estado` int(2) NOT NULL DEFAULT '1' COMMENT '1= activo\n0= inactivo',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`IdPermisosUsuarios`),
  KEY `fk_permisosusuarios_1_idx` (`usuarioId`),
  KEY `fk_permisosusuarios_2_idx` (`menuSistemaId`),
  CONSTRAINT `fk_permisosusuarios_1` FOREIGN KEY (`usuarioId`) REFERENCES `usuarios` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_permisosusuarios_2` FOREIGN KEY (`menuSistemaId`) REFERENCES `menusistema` (`idMenuSistema`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permisosusuarios`
--

LOCK TABLES `permisosusuarios` WRITE;
/*!40000 ALTER TABLE `permisosusuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `permisosusuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `idRol` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idRol`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'ADMINISTRDOR','ADMINISTRADOS DEL SISTEMA',NULL,NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `rolId` int(11) NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idUsuario`),
  KEY `fk_usuarios_1_idx` (`rolId`),
  CONSTRAINT `fk_usuarios_1` FOREIGN KEY (`rolId`) REFERENCES `roles` (`idRol`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,1,'admin','admin@hotmail.com','admin',NULL,'2018-09-08 19:20:25',NULL);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-08 18:52:35
