<?php 
/**
* 
*/
class Mlogin extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function LoginBD($username, $password)
    {
        $this->db->where('user', $username);
        $usuario = $this->db->get('usuarios')->row();

        if(!$usuario) {
            return false;
        }
        
        if(password_verify($password, $usuario->password)) {
            return $usuario;
        }
        
        return false;
    }
}