<?php 
/**
* 
*/
class Mviaje extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

    
	public function viajes( $id = null )
    {
    	if ( $id != null ) {

            $this->db->where('id', (int)$id);
            $this->db->from('viajes');
            return $this->db->get()->row();

        } else {

            $query = $this->db->query('SELECT * FROM viajes ORDER BY codigo');
        	return $query->result();
        }

        
    }

    public function insertar($data = [])
    {
        if($this->db->insert('viajes', $data)) {
            return TRUE;
        }
        return false;
    }

    public function editar($data = [])
    {
        $this->db->where('id', $data['id']);
        return $this->db->update('viajes', $data);
    }

    public function borrar($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('viajes');
    }

    public function verificar( $id )
    {
        if ( $id ) {

            $this->db->where('viaje_id', (int)$id);
            $this->db->from('viajero_viaje');
            if($this->db->get()->row()){
                return true;
            }else{
                return false;
            }
            

        } 

        
    }


}