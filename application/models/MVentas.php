<?php 
/**
* 
*/
class MVentas extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

    
	public function ventas( $id = null )
    {
    	if ( $id != null ) {

            $query = $this->db->query('
                SELECT * FROM 
                viajero_viaje 
                INNER JOIN 
                viajeros 
                ON 
                viajero_viaje.viajero_id = viajeros.id 
                INNER JOIN 
                viajes 
                ON 
                viajero_viaje.viaje_id = viajes.id
                WHERE
                viajero_viaje.viajero_id = "' . $id . '"
            ');
            
            return $query->result();

        } else {

            $query = $this->db->query('
                SELECT * FROM 
                viajero_viaje 
                INNER JOIN 
                viajeros 
                ON 
                viajero_viaje.viajero_id = viajeros.id 
                INNER JOIN 
                viajes 
                ON 
                viajero_viaje.viaje_id = viajes.id
                group by nombre
            ');

        	return $query->result();
        }

        
    }

    public function allVentas()
    {
        $query = $this->db->query('
            SELECT * FROM unoauno.viajero_viaje group by viajero_id
        ');
        
        return $query->result();
    }

    public function insertar($data = [])
    {
        return $this->db->insert_batch('viajero_viaje', $data);
    }

    public function borrar($id)
    {
        $this->db->where('viajero_id', $id);
        return $this->db->delete('viajero_viaje');
    }


}