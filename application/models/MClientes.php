<?php 
/**
* 
*/
class MClientes extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

    
	public function viajeros( $id = null )
    {
    	if ( $id != null ) {

            $this->db->where('id', (int)$id);
            $this->db->from('viajeros');
            return $this->db->get()->row();

        } else {

            $query = $this->db->query('SELECT * FROM viajeros');
        	return $query->result();
        }

        
    }

    public function insertar($data = [])
    {
        if($this->db->insert('viajeros', $data)) {
            return TRUE;
        }
        return false;
    }

    public function editar($data = [])
    {
        $this->db->where('id', $data['id']);
        return $this->db->update('viajeros', $data);
    }

    public function borrar($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('viajeros');
    }

    public function verificar( $id )
    {
        if ( $id ) {

            $this->db->where('viajero_id', (int)$id);
            $this->db->from('viajero_viaje');
            if($this->db->get()->row()){
                return true;
            }else{
                return false;
            }
            

        } 

        
    }


}