<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
*
*/
class Clogin extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
          $this->load->model('usuario/Mlogin');
		
	}

     public function index()
     {
          if ($this->session->is_logged_in != NULL ) {

               redirect( 'Cinicio' );

          }

          $vista['subview'] = 'login/index';
          $this->load->view('layout/main',$vista);
     }

	public function ValidarAcceso(){

          #Campos para validar que no esten vacio los campos
          $this->form_validation->set_rules("txtNomUsuario", "Usuario", "trim|required");
          $this->form_validation->set_rules("txtClave", "Password", "trim|required");

          #Campos para validar con la base de datos
          $username = $this->input->post('txtNomUsuario');
          $password = $this->input->post('txtClave');

          if ($this->form_validation->run() == true){

               $user = $this->Mlogin->LoginBD($username, $password);

               if ( $user != FALSE ) {

                    $session = array(
                         'id' => $user->id,
                         'username' => $user->user,                   
                         'is_logged_in' => TRUE,
                    );

                    $this->session->set_userdata($session);

                    redirect("Cinicio");

               }else{

                    $this->session->set_flashdata('msj','Usuario o Contraseña inválido');
                    redirect("Clogin");
               }

          }else{
               $this->session->set_flashdata('msj','No puede dejar campos vacios');
               redirect("Clogin");
          }
     }



	public function CerrarSesion(){

          #destrozamos la sesion activay nos vamos al login de nuevo
          if($this->session->userdata('is_logged_in')){
               
               $this->session->sess_destroy();
               redirect(base_url(""), "refresh");
          }
          else{

               redirect(base_url(""), "refresh");
          }
     }

}
