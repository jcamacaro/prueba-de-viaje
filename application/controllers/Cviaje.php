<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
*
*/
class Cviaje extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
        $this->load->model('Mviaje');

        #Helper para restringir acceso por URL
        new RestringirAcceso( $this->session->is_logged_in );		
	}

     public function index()
     {
        $vista['subview']   = 'viajes/index';
        $vista['js']        = 'viajes/js/script';
        $this->load->view('layout/main',$vista);          
     }

    #Arreglo para el datatable
     public function lista()
     {
          echo json_encode( $this->Mviaje->viajes() );
     }

     public function modalCrear()
     {
          $viaje = [
               "id" => '',
               "codigo" => '',
               "numeroPlaza" => '',
               "destino"     => '',
               "lugarOrigen" => '',
               "precio"      => '',
          ];

        $data['viaje'] = (object)$viaje;
        $this->load->view('viajes/formulario', $data);

     }

     public function crear()
    {      

        $dataViaje = [
          "codigo"       => $_POST['codigo'],
          "numeroPlaza"  => $_POST['numeroPlaza'],
          "destino"      => $_POST['destino'],
          "lugarOrigen"  => $_POST['lugarOrigen'],
          "precio"       => $_POST['precio'],           
        ];

          if($this->Mviaje->insertar($dataViaje)){
               echo json_encode([
                   'result' => true,
                   'mensaje' => 'Usuario creado correctamente'
               ]);    
          }
    }

    public function modalEditar()
    {
        $data['viaje'] = $this->Mviaje->viajes($_POST['id']);
        $data['editar'] = true;

        $this->load->view('viajes/formulario', $data);

    }

    public function editar()
    {

          $dataViaje = [
               'id' => $_POST['id'], 
               "codigo"       => $_POST['codigo'],
               "numeroPlaza"  => $_POST['numeroPlaza'],
               "destino"      => $_POST['destino'],
               "lugarOrigen"  => $_POST['lugarOrigen'],
               "precio"       => $_POST['precio'],           
        ];
      

          if(!$this->Mviaje->editar($dataViaje)) {
               echo json_encode([
                    'result' => false,
                    'mensaje' => 'Ocurrió un error al editar la persona',
               ]);
               exit;
          }

          echo json_encode([
            'result' => true,
            'mensaje' => 'Usuario creado correctamente'
          ]);
    }

    public function borrar()
    {
        #Funcion para verificar si se encuentra relacionado
        if ($this->Mviaje->verificar($_POST['id']) == TRUE ) {
            
            echo json_encode([
                'result' => false,
                'mensaje' => 'No se puede eliminar ya que tiene algun viajero asociado'
            ]);
            exit;

        } else {
            
            if(!$this->Mviaje->borrar($_POST['id'])) {
               
                echo json_encode([
                    'result' => false,
                    'mensaje' => 'Ocurrió un error al borrar el viaje'
                ]);
                exit;
            }else{
                 echo json_encode([
                      'result' => true,
                      'mensaje' => 'Destino borrado correctamente'
                 ]);
            } 
        }     
    }


}
