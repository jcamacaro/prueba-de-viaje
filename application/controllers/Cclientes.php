<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cclientes extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
        $this->load->model('MClientes');

        #Helper para restringir acceso por URL
        new RestringirAcceso( $this->session->is_logged_in );
		
	}

    public function index()
    {
        $vista['subview']   = 'clientes/index';
        $vista['js']        = 'clientes/js/script';
        $this->load->view('layout/main',$vista);          
    }

    #Arreglo para el datatable
    public function lista()
    {
        echo json_encode( $this->MClientes->viajeros() );
    }

    public function modalCrear()
    {
        $cliente = [
           "id" => '',
           "cedula" => '',
           "nombre" => '',
           "direccion"     => '',
           "telefono" => '',
        ];

        $data['cliente'] = (object)$cliente;
        $this->load->view('clientes/formulario', $data);

    }

    public function crear()
    {      

        $dataCliente = [
          "cedula"       => $_POST['cedula'],
          "nombre"  => $_POST['nombre'],
          "direccion"      => $_POST['direccion'],
          "telefono"  => $_POST['telefono'],         
        ];

        if($this->MClientes->insertar($dataCliente)){
            echo json_encode([
                'result' => true,
                'mensaje' => 'Usuario creado correctamente'
            ]);    
        }
    }

    public function modalEditar()
    {
        $data['cliente'] = $this->MClientes->viajeros($_POST['id']);
        $data['editar'] = true;

        $this->load->view('clientes/formulario', $data);

    }

    public function editar()
    {

       $dataCleinte = [
            'id' => $_POST['id'], 
            "cedula"       => $_POST['cedula'],
            "nombre"  => $_POST['nombre'],
            "direccion"      => $_POST['direccion'],
            "telefono"  => $_POST['telefono'],
    ];
      

    if(!$this->MClientes->editar($dataCleinte)) {
        echo json_encode([
            'result' => false,
            'mensaje' => 'Ocurrió un error al editar la persona',
        ]);
        exit;
    }

        echo json_encode([
            'result' => true,
            'mensaje' => 'Usuario creado correctamente'
        ]);
    }

    public function borrar()
    {
        #Funcion para verificar si se encuentra relacionado
        if ($this->MClientes->verificar($_POST['id']) == TRUE ) {
            
            echo json_encode([
                'result' => false,
                'mensaje' => 'No se puede eliminar ya que tiene algun viaje asociado'
            ]);
            exit;

        } else {
            
            if(!$this->MClientes->borrar($_POST['id'])) {
               
                echo json_encode([
                    'result' => false,
                    'mensaje' => 'Ocurrió un error al borrar el cliente'
                ]);
                exit;
            }else{
                 echo json_encode([
                      'result' => true,
                      'mensaje' => 'Cliente borrado correctamente'
                 ]);
            } 
        }
               
    }


}
