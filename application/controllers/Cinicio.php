<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cinicio extends CI_Controller {
	
	function __construct(){
        
        parent::__construct();

        #Helper para restringir acceso por URL
        new RestringirAcceso( $this->session->is_logged_in );
    }
	
		
	public function index()
	{
		$vista['subview']   = 'inicio/index';
        $this->load->view('layout/main',$vista);
	}
}
?>