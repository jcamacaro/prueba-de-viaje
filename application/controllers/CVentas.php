<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
*
*/
class CVentas extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
        $this->load->model('MVentas');
        $this->load->model('MClientes');
        $this->load->model('Mviaje');

        #Helper para restringir acceso por URL
        new RestringirAcceso( $this->session->is_logged_in );        
		
	}

     public function index()
     {
        $vista['subview']   = 'ventas/index';
        $vista['js']        = 'ventas/js/script';
        $this->load->view('layout/main',$vista);          
     }

     #Arreglo para el datatable
     public function lista()
     {
        echo json_encode( $this->MVentas->ventas() );
     }

     public function modalCrear()
     {
       
        $viajes   = $this->Mviaje->viajes();
        $clientes = $this->MClientes->viajeros();
        $ventas   = $this->MVentas->allVentas();
        
        #proceso para solo mostrar los clientes que no tengan viajes
        foreach ($clientes as $cliente) {
            
            $asignado = false;

            foreach ($ventas as $venta) {
                
                if ($cliente->id == $venta->viajero_id) {
                    $asignado = true;
                } 
                
            }

            if ($asignado == false) {
                
                $clienteSinViaje[] = $cliente;
            }

        }

        if ( isset($clienteSinViaje) ) {
            $data['clienteSinViaje'] = $clienteSinViaje;
        }else{
            $data['clienteSinViaje'] = "";
        }


        $data['viajes'] = $viajes;
        $data['clientes'] = $clientes;
        $data['ventas'] = $ventas;
        $this->load->view('ventas/formulario', $data);

     }

     public function crear()
    {    

        $dataViajesCliente = [];

        if($_POST['viajes']) {

            foreach($_POST['viajes'] as $viaje) {
                $dataViajesCliente[] = 
                    [
                        'viajero_id' => $_POST['cliente'],
                        'viaje_id' => $viaje
                    ];
            }

        }  

        if($this->MVentas->insertar($dataViajesCliente)){
            echo json_encode([
                'result' => true,
                'mensaje' => 'Se insertaron los viajes de cliente'
            ]);   
        }

    }

    public function modalEditar()
    {
        $clientes = $this->MVentas->ventas($_POST['id']);
        $viajes   = $this->Mviaje->viajes();

        $data['editar'] = true;
        $data['viajes'] = $viajes;
        $data['clientes'] = $clientes;

        $this->load->view('ventas/formulario', $data);

    }

    public function editar()
    {

        $dataViajesCliente = [];

        if($_POST['viajes']) {

            foreach($_POST['viajes'] as $viaje) {
                $dataViajesCliente[] = 
                    [
                        'viajero_id' => $_POST['cliente'],
                        'viaje_id' => $viaje
                    ];
            }

        }

        if($this->MVentas->borrar($_POST['cliente'])) {

            if($this->MVentas->insertar($dataViajesCliente)){
                echo json_encode([
                    'result' => true,
                    'mensaje' => 'Se insertaron los viajes de cliente'
                ]);   
            }

        }


    }

    public function borrar()
    {

        if(!$this->MVentas->borrar($_POST['id'])) {
             
           echo json_encode([
                'result' => false,
                'mensaje' => 'Ocurrió un error al borrar el viaje'
            ]);
            exit;
        }else{
           echo json_encode([
                'result' => true,
                'mensaje' => 'Cliente borrado correctamente con sus viajes'
           ]);
        }        
    }

    public function modalVer()
    {
        $clientes = $this->MVentas->ventas($_POST['id']);
        $viajes   = $this->Mviaje->viajes();

        $data['editar'] = true;
        $data['viajes'] = $viajes;
        $data['clientes'] = $clientes;

        $this->load->view('ventas/lista', $data);

    }


}
