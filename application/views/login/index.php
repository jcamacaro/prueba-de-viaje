
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="#"><b>Uno A Uno</b></a>

      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Identifíquese para iniciar sesión</p>
        <form action="<?php echo site_url(); ?>/Clogin/ValidarAcceso" method="POST">
          <div class="form-group has-feedback">
            <input type="text" name="txtNomUsuario" class="form-control" placeholder="Usuario" maxlength="10" autofocus>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="txtClave" class="form-control" placeholder="Contraseña">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>

            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
            </div><!-- /.col -->
          </div>
        </form>

        <?php if ($this->session->flashdata('msj')): ?>
          <?= $this->session->flashdata('msj') ?>
        <?php endif; ?>
    
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

  </body>
</html>
