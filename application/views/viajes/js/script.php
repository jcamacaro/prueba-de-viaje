<script>
$("#tablaViaje").DataTable({
    'ajax': {
        "url": '<?=site_url()?>/Cviaje/lista',
        "type": "GET",
        dataSrc: '',
    },
    'columns': [
        {data: 'codigo', className:"text-center"},
        {data: 'numeroPlaza', className:"text-center"},
        {data: 'destino', className:"text-center"},
        {data: 'lugarOrigen', className:"text-center"},
        {data: 'precio', className:"text-center"},
        {
            render: function (data, type, row) {
                var botones = '';                
                
                botones += '<button data-toggle="tooltip" title="Editar" onclick="modalEditar('+row.id+')" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></button>';                
                
                botones += '<button data-toggle="tooltip" title="Borrar" onclick="borrarViaje('+row.id+')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>';
                
                return botones;
            }
        }
    ]
});

function modalCrear() {
    $.ajax({
        url: '<?=site_url()?>/Cviaje/modalCrear',
        type: 'POST',
        data: {modal: true},
        success: function(response) {
            $('#contenidoModalFormulario').empty();
            $('#contenidoModalFormulario').html(response);

            $('#tituloModalFormulario').text('Crear Viaje');

            $('#modalFormulario').modal();
        },
        error: function() {
            alert('Ocurrió un error');
        }
    });
}

function modalEditar(id) {
    $.ajax({
        url: '<?=site_url()?>/Cviaje/modalEditar',
        type: 'POST',
        data: {
            id: id,
            modal: true,
        },
        success: function(response) {
            $('#contenidoModalFormulario').empty();
            $('#contenidoModalFormulario').html(response);

            $('#tituloModalFormulario').text('Editar Viaje');

            $('#modalFormulario').modal();
        },
        error: function() {
            alert('Ocurrió un error');
        }
    });
}

function borrarViaje(id) {
    swal.queue([{
      title: '¿Seguro que desea borrar este Viaje?',
      showCancelButton: true,
      type: 'warning',
      confirmButtonText: 'Sí, borrar',
      cancelButtonText: 'Cancelar',
      text: 'Esta acción no se puede revertir',
      showLoaderOnConfirm: true,
      preConfirm: () => {

        return $.ajax({
            type: "POST",
            url: '<?=site_url()?>/Cviaje/borrar',
            dataType: 'json',
            data: {
              id: id,
            },
            success: function(data)
            {
                if(data.result) {
                     swal.insertQueueStep({
                        type: 'success',
                        title: data.mensaje
                     });
                     $('#tablaViaje').DataTable().ajax.reload();
                } else {
                    swal.insertQueueStep({
                        type: 'error',
                        title: data.mensaje
                    });
                }
            },
            error: function() {
                swal.insertQueueStep({
                    type: 'error',
                    title: 'Ocurrió un error al borrar el Viaje'
                });
            }
          });
      }
    }]);
}
</script>


