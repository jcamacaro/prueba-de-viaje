<form method="POST" action="menus/crear" onsubmit="guardar(event)">
    <input type="hidden" id="id" value="<?= $viaje->id ?>">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="codigo">Código:</label>
                <input
                    type="text"
                    class="form-control"
                    id="codigo"
                    value="<?= $viaje->codigo ?>"
                />
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="numeroPlaza">Número Plaza:</label>
                <input
                    type="text"
                    class="form-control"
                    id="numeroPlaza"
                    min="0"
                    value="<?= $viaje->numeroPlaza ?>"
                />
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label for="destino">Destino:</label>
                <input
                    type="text"
                    class="form-control"
                    id="destino"
                    value="<?= $viaje->destino ?>"
                />
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label for="lugarOrigen">Lugar Origen:</label>
                <input
                    type="text"
                    class="form-control"
                    id="lugarOrigen"
                    value="<?= $viaje->lugarOrigen ?>"
                />
            </div>
        </div>
    </div>

    <div class="row">
        

        <div class="col-sm-6">
            <div class="form-group">
                <label for="precio">Precio:</label>
                <input
                    type="text"
                    class="form-control"
                    id="precio"
                    min="0"
                    value="<?= $viaje->precio ?>"
                />
            </div>            
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12">
            <button class="btn btn-primary pull-right" type="submit">
                <i class="fa fa-save"></i> Guardar
            </button>
        </div>
    </div>

    <br>

</form>

<script type="text/javascript">

function guardar(e) {

    e.preventDefault();

    <?php if(isset($editar) && $editar): ?>
        var url = '<?=site_url()?>/Cviaje/editar'
    <?php else: ?>
        var url = '<?=site_url()?>/Cviaje/crear'
    <?php endif; ?>

    var id      = $('#id').val();
    var codigo  = $('#codigo').val();
    var numeroPlaza = $('#numeroPlaza').val();
    var destino     = $('#destino').val();
    var lugarOrigen = $('#lugarOrigen').val();
    var precio      = $('#precio').val();

    if( codigo == "" | numeroPlaza == "" | destino == "" | lugarOrigen == "" | precio == "") {
        swal.queue([{
            title: 'Debe Llenar todos los campos',
            type: 'warning',
            confirmButtonText: 'Okey',              
            showLoaderOnConfirm: true,
            preConfirm: () => {
                $('#tablaViaje').DataTable().ajax.reload();            
            }
        }]);

    }else{
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {
                id: id,
                codigo: codigo,
                numeroPlaza: numeroPlaza,
                destino: destino,
                lugarOrigen: lugarOrigen,                        
                precio: precio,            
            },
            success: function(data) {
                if(data.result) {
                    
                    swal.queue([{
                        title: 'Guardado Correctamente',
                        type: 'success',
                        confirmButtonText: 'Okey',              
                        showLoaderOnConfirm: true,
                        preConfirm: () => {
                            $('#modalFormulario').modal('toggle');
                            $('#tablaViaje').DataTable().ajax.reload();
                        }
                    }]);
                    
                } else {
                    alert(data.mensaje);
                }
            },
            error: function() {
                alert('Ocurrió un error');
            }
        })  
    }

    
}

</script>
