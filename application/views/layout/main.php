<?php
	if ( is_null($this->session->is_logged_in) ) {

		#Cabecera
		$this->load->view('layout/login/header');
		#Contenido
		$this->load->view($subview);
		#Pie de página
		$this->load->view('layout/login/footer');

	} else {

		#Cabecera
		$this->load->view('layout/pageHead');
		#Cabecera del cuerpo
		$this->load->view('layout/pageBodyHeader');
		#Navegacion
		$this->load->view('layout/pageNavegation');	
	 	#Contenido
	 	$this->load->view($subview);
	 	#Pie de página
		$this->load->view('layout/pageTail');
		#Script necesarios
		if( isset($js) ){
			$this->load->view($js);
		}

	}


 ?>