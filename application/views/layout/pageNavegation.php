<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
    	<!-- Sidebar user panel -->
    	<?php if ( $this->session->userdata('is_logged_in') ): ?>

    		<div class="user-panel">
                <div class="pull-left image">
                  <img src="assets/dist/img/UNP-Logo.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                  <p> <?= $this->session->userdata('username'); ?> </p>
                  <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <!-- Menu codeado -->
                <ul class="sidebar-menu">  
                    <li class="header" style="color: #e0e7ea; background: rgb(44, 59, 65);"> ADMINISTRACIÓN </li>  

                    <li class="treeview">

                        <a href="#">
                            <i class="fa fa-fw fa-plus-square"></i> 
                            <span> Plan </span>
                        </a>

                        <ul class="treeview-menu">

                            <li>
                                <a href="Cviaje/index">
                                    <i class="fa fa-fw fa-plus-square"></i>Viajes  
                                </a>
                            </li>

                            <li>
                                <a href="Cclientes/index">
                                    <i class="fa fa-fw fa-plus-square"></i>Clientes  
                                </a>
                            </li>

                            <li>
                                <a href="CVentas/index">
                                    <i class="fa fa-fw fa-plus-square"></i>Ventas  
                                </a>
                            </li>

                        </ul>

                    </li> 
                </ul>
            <!-- END Menu codeado -->

    	<?php endif ?>
    </section>

    <!-- /.sidebar -->
</aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- <section class="content-header">
         
    </section> -->

    <!-- Main content -->
    <section class="content">