                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Prueba Viaje</b> 1.0
                </div>
                    
                <strong> <a href="#">©Ing Jesus Camacaro&nbsp;|&nbsp;</a></h6></strong> Todos los derechos reservados.
            </footer>    
              
        </div><!-- ./wrapper -->

        <!-- View_home -->
        <script src="assets/mapsvg/js/jquery.mousewheel.min.js"></script>
        <script src="assets/mapsvg/js/mapsvg.min.js"></script>

        <!-- Chosen select -->
        <script src="assets/plugins/chosen/chosen.jquery.min.js"></script>

        <!-- End View_home -->
        <script src="assets/plugins/datatables/jquery.dataTables.js"></script>
        <script src="assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
           
        <!-- daterangepicker -->
        <script src="assets/plugins/moment/moment.min.js"></script>
        <script src="assets/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <script src="assets/plugins/datepicker/locales/bootstrap-datepicker.es.js"></script>
        <!-- FORMATO DE NUMEROS -->
        <script src="assets/plugins/numeraljs/numeral.min.js"> </script>
            
        <!-- AdminLTE App -->
        <script src="assets/dist/js/app.min.js"></script>

        <!-- Sweetalert -->
        <script src="assets/plugins/sweetalert/sweetalert2.min.js"></script>

        <!-- Bootstrap Fileinput -->
        <script src="assets/plugins/bootstrap-fileinput/js/fileinput.min.js"></script>
    </body>
</html>

