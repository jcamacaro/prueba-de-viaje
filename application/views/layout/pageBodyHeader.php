<body class="hold-transition skin-blue sidebar-mini">
  
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="clogin/" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels-->
                <span class="logo-mini"><b>UAU</b></span> 
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"> <b>Uno A Uno</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?= $this->session->userdata('foto') ? $this->session->userdata('foto') : 'assets/pics/default.jpg' ?>" class="user-image" alt="User Image">
                                <span class="hidden-xs"><?= $this->session->userdata('username');?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?= $this->session->userdata('foto') ? $this->session->userdata('foto') : 'assets/pics/default.jpg' ?>" class="img-circle" alt="User Image">
                                    <p>
                                      <?= $this->session->userdata('username')."<br/>"?>
                                      
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="Clogin/cerrarSesion" class="btn btn-default btn-flat"><i class="fa fa-power-off"></i> &nbsp;Cerrar Sesion</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>