<!DOCTYPE html>
<html lang="es" xml:lang="es">
  <head>
    <base href="<?= base_url(); ?>"/>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Uno A Uno</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <script src="assets/mapsvg/js/jquery.js"></script>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">

    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap_lk.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
    <!-- Theme style -->
    <link rel="stylesheet" href="assets/dist/css/AdminLTE_lk.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="assets/dist/css/skins/_all-skins-lk.css">
    
    <!-- Date Picker -->
    <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <!-- <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker-bs3.css">-->
    <!-- bootstrap wysihtml5 - text editor -->

    <link rel="stylesheet" href="assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- Datatables -->
    <link rel="stylesheet" href="assets/datatables/css/jquery.dataTables.css">

    <link rel="shortcut icon" href="assets/is.ico" type="image/ico" />

    <!-- Chosen select -->
    <link rel="stylesheet" href="assets/plugins/chosen/chosen.min.css">

    <!-- Sweetalert -->
    <link rel="stylesheet" href="assets/plugins/sweetalert/sweetalert2.min.css">

    <!-- Bootstrap Fileinput -->
    <link rel="stylesheet" href="assets/plugins/bootstrap-fileinput/css/fileinput.min.css">


    <!-- ******************** MAPA************ -->
    <link href="assets/mapsvg/css/mapsvg.css" rel="stylesheet">    
  </head>

  