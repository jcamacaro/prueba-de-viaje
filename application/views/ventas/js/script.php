<script>
$("#tablaVentas").DataTable({
    'ajax': {
        "url": '<?=site_url()?>/CVentas/lista',
        "type": "GET",
        dataSrc: '',
    },
    'columns': [
        {data: 'nombre', className:"text-center"},
        {
            render: function (data, type, row) {
                var botones = '';                

                botones += '<button data-toggle="tooltip" title="Ver" onclick="modalVer('+row.viajero_id+')" class="btn btn-success btn-xs"><i class="fa fa-list"></i></button>';
                
                botones += '<button data-toggle="tooltip" title="Editar" onclick="modalEditar('+row.viajero_id+')" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></button>';                
                
                botones += '<button data-toggle="tooltip" title="Borrar" onclick="borrarViaje('+row.viajero_id+')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>';


                
                return botones;
            }
        }
    ]
});

function modalCrear() {
    $.ajax({
        url: '<?=site_url()?>/CVentas/modalCrear',
        type: 'POST',
        data: {modal: true},
        success: function(response) {
            $('#contenidoModalFormulario').empty();
            $('#contenidoModalFormulario').html(response);

            $('#tituloModalFormulario').text('Crear Venta');

            $('#modalFormulario').modal();
        },
        error: function() {
            alert('Ocurrió un error');
        }
    });
}

function modalEditar(id) {
    $.ajax({
        url: '<?=site_url()?>/CVentas/modalEditar',
        type: 'POST',
        data: {
            id: id,
            modal: true,
        },
        success: function(response) {
            $('#contenidoModalFormulario').empty();
            $('#contenidoModalFormulario').html(response);

            $('#tituloModalFormulario').text('Editar Venta');

            $('#modalFormulario').modal();
        },
        error: function() {
            alert('Ocurrió un error');
        }
    });
}

function borrarViaje(id) {
    swal.queue([{
      title: '¿Seguro que desea borrar todos los viajes del cliente?',
      showCancelButton: true,
      type: 'warning',
      confirmButtonText: 'Sí, borrar',
      cancelButtonText: 'Cancelar',
      text: 'Esta acción no se puede revertir',
      showLoaderOnConfirm: true,
      preConfirm: () => {

        return $.ajax({
            type: "POST",
            url: '<?=site_url()?>/CVentas/borrar',
            dataType: 'json',
            data: {
              id: id,
            },
            success: function(data)
            {
                if(data.result) {
                     swal.insertQueueStep({
                        type: 'success',
                        title: data.mensaje
                     });
                     $('#tablaVentas').DataTable().ajax.reload();
                } else {
                    swal.insertQueueStep({
                        type: 'error',
                        title: data.mensaje
                    });
                }
            },
            error: function() {
                swal.insertQueueStep({
                    type: 'error',
                    title: 'Ocurrió un error al borrar el Viaje'
                });
            }
          });
      }
    }]);
}

function modalVer(id) {
    $.ajax({
        url: '<?=site_url()?>/CVentas/modalVer',
        type: 'POST',
        data: {
            id: id,
            modal: true,
        },
        success: function(response) {
            $('#contenidoModalFormulario').empty();
            $('#contenidoModalFormulario').html(response);

            $('#tituloModalFormulario').text('Ver Venta');

            $('#modalFormulario').modal();
        },
        error: function() {
            alert('Ocurrió un error');
        }
    });
}
</script>


