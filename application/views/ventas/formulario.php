<form method="POST" action="usuarios/crear" onsubmit="guardar(event)">
    
    <div class="row">

        <?php if(isset($editar) && $editar): ?>
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Cliente:</label>
                    <select
                        data-placeholder="Seleccione El Cliente"
                        class="form-control"
                        id="cliente"
                    >
                        <?php foreach($clientes as $cliente): ?>
                            <option
                                value="<?= $cliente->viajero_id ?>"
                            >
                                Nombre: <?= $cliente->nombre ?> - Cédula: <?= $cliente->cedula ?>
                            </option>
                            <?php break; ?>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        <?php else: ?>
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Cliente:</label>
                    <select
                        data-placeholder="Seleccione El Cliente"
                        class="form-control"
                        id="cliente"
                    >
                        <option value="" > Seleccione Cliente </option>
                        <?php if( $clienteSinViaje != ""): ?>
                            <?php foreach($clienteSinViaje as $cliente): ?>
                                <option
                                    value="<?= $cliente->id ?>"
                                >
                                    Nombre: <?= $cliente->nombre ?> - Cédula: <?= $cliente->cedula ?>
                                </option>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <option value="" > No hay Cliente para viajes </option>
                        <?php endif ?>
                    </select>
                </div>
            </div>
        <?php endif; ?>
        

        <?php if(isset($editar) && $editar): ?>

            <div class="col-sm-12">
                <div class="form-group">
                    <label>Viajes:</label>
                    <select
                        data-placeholder="Seleccione los viajes"
                        class="form-control"
                        id="viajes"
                        multiple
                    >
                        <?php foreach($viajes as $viaje): ?>
                            <?php foreach($clientes as $cliente): ?>
                                <option
                                    value="<?= $viaje->id ?>"
                                    <?= $viaje->id == $cliente->viaje_id ? 'selected' : '' ?>
                                >
                                    Destino: <?= $viaje->destino ?> - Lugar de Origen: <?= $viaje->lugarOrigen ?> - Precio: <?= $viaje->precio ?>
                                </option>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

        <?php else: ?>

            <div class="col-sm-12">
                <div class="form-group">
                    <label>Viajes:</label>
                    <select
                        data-placeholder="Seleccione los viajes"
                        class="form-control"
                        id="viajes"
                        multiple
                    >
                        <?php foreach($viajes as $viaje): ?>
                            <option
                                value="<?= $viaje->id ?>"
                            >
                                Destino: <?= $viaje->destino ?> - Lugar de Origen: <?= $viaje->lugarOrigen ?> - Precio: <?= $viaje->precio ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            
        <?php endif; ?>
        
        

    </div>

    <div class="row">
        <div class="col-sm-12">
            <button class="btn btn-primary pull-right" type="submit">
                <i class="fa fa-save"></i> Guardar
            </button>
        </div>
    </div>

    <br>

</form>

<script type="text/javascript">


$(document).ready(function() {


    $('#viajes').chosen({
        width: '100%',
    });

    $('#cliente').chosen({
        width: '100%',
    });
});


function guardar(e) {

    e.preventDefault();

    <?php if(isset($editar) && $editar): ?>
        var url = '<?=site_url()?>/CVentas/editar'
    <?php else: ?>
        var url = '<?=site_url()?>/CVentas/crear'
    <?php endif; ?>

    var cliente =  $('#cliente').chosen().val();
    var viajes = $('#viajes').chosen().val();

    if ( cliente == "" | viajes =="") {

        swal.queue([{
            title: 'Debe completar los campos',
            type: 'warning',
            confirmButtonText: 'Okey',              
            showLoaderOnConfirm: true,
            preConfirm: () => {
                $('#modalFormulario').modal('toggle');
                $('#tablaVentas').DataTable().ajax.reload();
            }
        }]);

    }else {
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {            
                cliente: $('#cliente').chosen().val(),
                viajes: $('#viajes').chosen().val(),
            },
            success: function(data) {
                if(data.result) {

                    swal.queue([{
                        title: 'Guardado Correctamente',
                        type: 'success',
                        confirmButtonText: 'Okey',              
                        showLoaderOnConfirm: true,
                        preConfirm: () => {
                            $('#modalFormulario').modal('toggle');
                            $('#tablaVentas').DataTable().ajax.reload();
                        }
                    }]);

                } else {
                    alert(data.mensaje);
                }
            },
            error: function() {
                alert('Ocurrio un error');
            }
        })
    }

    
}

</script>
