
    
    <div class="row">
        <div class="col-sm-12">
            <table id="tablaViaje" class="table table-bordered table-striped">
                <caption>Viajes del Cliente</caption>
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Número Plaza</th>
                        <th>Destino</th>
                        <th>Lugar Origen</th>
                        <th>Precio</th>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach($viajes as $viaje): ?>
                        <?php foreach($clientes as $cliente): ?>
                            <?php if($viaje->id == $cliente->viaje_id): ?>
                                <tr>
                                    <td><?= $viaje->codigo ?></td>
                                    <td><?= $viaje->numeroPlaza ?></td>
                                    <td><?= $viaje->destino ?> </td>
                                    <td><?= $viaje->lugarOrigen ?></td>
                                    <td><?= $viaje->precio ?></td>
                                </tr>
                            <?php endif; ?> 
                        <?php endforeach; ?>
                    <?php endforeach; ?>

                </tbody>
        
            </table> 
        </div>
        

        
    </div>

    <br>



