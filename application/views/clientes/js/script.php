<script>
$("#tablaCliente").DataTable({
    'ajax': {
        "url": '<?=site_url()?>/Cclientes/lista',
        "type": "GET",
        dataSrc: '',
    },
    'columns': [
        {data: 'cedula', className:"text-center"},
        {data: 'nombre', className:"text-center"},
        {data: 'direccion', className:"text-center"},
        {data: 'telefono', className:"text-center"},
        {
            render: function (data, type, row) {
                var botones = '';                
                
                botones += '<button data-toggle="tooltip" title="Editar" onclick="modalEditar('+row.id+')" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></button>';                
                
                botones += '<button data-toggle="tooltip" title="Borrar" onclick="borrarViaje('+row.id+')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>';
                
                return botones;
            }
        }
    ]
});

function modalCrear() {
    $.ajax({
        url: '<?=site_url()?>/Cclientes/modalCrear',
        type: 'POST',
        data: {modal: true},
        success: function(response) {
            $('#contenidoModalFormulario').empty();
            $('#contenidoModalFormulario').html(response);

            $('#tituloModalFormulario').text('Crear Cliente');

            $('#modalFormulario').modal();
        },
        error: function() {
            alert('Ocurrió un error');
        }
    });
}

function modalEditar(id) {
    $.ajax({
        url: '<?=site_url()?>/Cclientes/modalEditar',
        type: 'POST',
        data: {
            id: id,
            modal: true,
        },
        success: function(response) {
            $('#contenidoModalFormulario').empty();
            $('#contenidoModalFormulario').html(response);

            $('#tituloModalFormulario').text('Editar Cliente');

            $('#modalFormulario').modal();
        },
        error: function() {
            alert('Ocurrió un error');
        }
    });
}

function borrarViaje(id) {
    swal.queue([{
      title: '¿Seguro que desea borrar este cliente?',
      showCancelButton: true,
      type: 'warning',
      confirmButtonText: 'Sí, borrar',
      cancelButtonText: 'Cancelar',
      text: 'Esta acción no se puede revertir',
      showLoaderOnConfirm: true,
      preConfirm: () => {

        return $.ajax({
            type: "POST",
            url: '<?=site_url()?>/Cclientes/borrar',
            dataType: 'json',
            data: {
              id: id,
            },
            success: function(data)
            {
                if(data.result) {
                     swal.insertQueueStep({
                        type: 'success',
                        title: data.mensaje
                     });
                     $('#tablaCliente').DataTable().ajax.reload();
                } else {
                    swal.insertQueueStep({
                        type: 'error',
                        title: data.mensaje
                    });
                }
            },
            error: function() {
                swal.insertQueueStep({
                    type: 'error',
                    title: 'Ocurrió un error al borrar el cliente'
                });
            }
          });
      }
    }]);
}
</script>


