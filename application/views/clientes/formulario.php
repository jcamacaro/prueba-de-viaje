<form method="POST" action="menus/crear" onsubmit="guardar(event)">
    <input type="hidden" id="id" value="<?= $cliente->id ?>">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="cedula">Cédula:</label>
                <input
                    type="text"
                    class="form-control"
                    id="cedula"
                    value="<?= $cliente->cedula ?>"
                    onkeypress="return valida(event);"
                />
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="nombre">Nombre:</label>
                <input
                    type="text"
                    class="form-control"
                    id="nombre"
                    min="0"
                    value="<?= $cliente->nombre ?>"
                />
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label for="direccion">Dirección:</label>
                <input
                    type="text"
                    class="form-control"
                    id="direccion"
                    value="<?= $cliente->direccion ?>"
                />
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label for="telefono">Teléfono:</label>
                <input
                    type="text"
                    class="form-control"
                    id="telefono"
                    value="<?= $cliente->telefono ?>"
                    onkeypress="return valida(event);"
                />
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12">
            <button class="btn btn-primary pull-right" type="submit">
                <i class="fa fa-save"></i> Guardar
            </button>
        </div>
    </div>

    <br>

</form>

<script type="text/javascript">

    function valida(e){
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla==8){
            return true;
        }
            
        // Patron de entrada, en este caso solo acepta numeros
        patron =/[0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }

function guardar(e) {

    e.preventDefault();

    <?php if(isset($editar) && $editar): ?>
        var url = '<?=site_url()?>/Cclientes/editar'
    <?php else: ?>
        var url = '<?=site_url()?>/Cclientes/crear'
    <?php endif; ?>

    var id      = $('#id').val();
    var cedula  = $('#cedula').val();
    var nombre = $('#nombre').val();
    var direccion     = $('#direccion').val();
    var telefono = $('#telefono').val();;

    if( cedula == "" | nombre == "" | direccion == "" | telefono == "" ) {
        swal.queue([{
            title: 'Debe Llenar todos los campos',
            type: 'warning',
            confirmButtonText: 'Okey',              
            showLoaderOnConfirm: true,
            preConfirm: () => {
                $('#tablaCliente').DataTable().ajax.reload();            
            }
        }]);

    }else{
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {
                id: id,
                cedula: cedula,
                nombre: nombre,
                direccion: direccion,
                telefono: telefono,        
            },
            success: function(data) {
                if(data.result) {
                    
                    swal.queue([{
                        title: 'Guardado Correctamente',
                        type: 'success',
                        confirmButtonText: 'Okey',              
                        showLoaderOnConfirm: true,
                        preConfirm: () => {
                            $('#modalFormulario').modal('toggle');
                            $('#tablaCliente').DataTable().ajax.reload();
                        }
                    }]);
                    
                } else {
                    alert(data.mensaje);
                }
            },
            error: function() {
                alert('Ocurrió un error');
            }
        })  
    }

    
}

</script>
