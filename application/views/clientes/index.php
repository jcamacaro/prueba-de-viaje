<div class="box">
    
    <div class="box-header">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="box-title">Listado de Clientes</h3>
            </div>
            <div class="col-sm-6">
                
                    <button
                        onclick="modalCrear()"
                        type="button"
                        class="btn btn-primary pull-right">
                        <i class="fa fa-plus"></i> Nuevo Cliente
                    </button>
                
            </div>
        </div>
    </div>
<!-- /.box-header -->
<div class="box-body">
    <table id="tablaCliente" class="table table-bordered table-striped">
      <thead>
          <tr>
            <th>Cédula</th>
            <th>Nombre</th>
            <th>Dirección</th>
            <th>Teléfono</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>

    </tbody>
    <tfoot>
      <tr>
        <th>Cédula</th>
        <th>Nombre</th>
        <th>Dirección</th>
        <th>Teléfono</th>
        <th>Opciones</th>
    </tr>
</tfoot>
</table>
</div>
<!-- /.box-body -->
</div>

<!-- Modal -->
<div id="modalFormulario" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="tituloModalFormulario">Modal Header</h4>
            </div>
            <div class="modal-body" id="contenidoModalFormulario">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>

    </div>
</div>