<div class="row">
        <div class="col-md-12">
          <!-- Acceso-->
           
          <?php if ($this->session->flashdata('error')): ?>
           
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              <strong> <?= $this->session->flashdata('error') ?> </strong>
            </div>
            
          <?php endif; ?>

          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Prueba de viaje</font></font></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12" style="min-height: 280px;">
                        <p style="text-align: justify;">
                            Una agencia de viajes desea automatizar toda la gestión de los clientes que acuden a su
                            oficina y los viajes que estos realizan. La agencia proporciona la siguientes
                            requerimientos e información de interés:
                        </p>
                        <ul>
                            <li>
                                Se desea guardar la siguiente información de los viajeros: cédula, nombre,dirección y teléfono.
                            </li>

                            <li>
                                Se desea agregar información de viajes disponibles para ofrecer a los clientes: código de viaje, número de plazas, destino , lugar de origen y precio.
                            </li>

                            <li>
                                Un viajero puede realizar tantos viajes como desee.
                            </li>
                        </ul>

                        <p> <strong>Se quiere:</strong> </p>

                        <ul>
                            <li>Crear modelo entidad relación</li>
                            <li>
                                Desarrollar un mini proyecto que contenga
                                <ul>
                                    <li>
                                        CRUD de viajeros
                                    </li>
                                    <li>
                                        CRUD de viajes
                                    </li>
                                    <li>
                                        CRUD para relacionar viajeros con viajes
                                    </li>
                                </ul>
                            </li>

                            <li>
                                Se debe usar Bootstrapp y Jquery para manipular la interfaz gráfica
                            </li>

                            <li>
                                Todo debe estar programador con POO y MVC
                            </li>
                        </ul>
                        
                    </div>
                    <!-- /.col -->                
                </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              <div class="row">
                
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

